# Questions

Qs1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
the output will be:
```
2 1
```
setTimeout is a non blocking function; when called it will be pushed into the JS Event loop for execution
after the minimum threshold of 100ms has elapsed. As such, the setTimeout is held in the event loop allowing the
 second statement to run without pause.

Qs2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

The output of the above will be:
```
10 9 8 7 6 5 4 3 2 1 0 undefined
```
This is an example of recursive function call with a depth of 10. This creates an execution stack that is traversed until the condition of d < 10 is not met. At this point, the context and possible return values are then 'rewound' back up to the original function call i.e. foo(0).


Qs3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

If a falsy d value/object is passed, 5 will always be set to d. Conversely if a more truthy value/object is passed, it will supersede 5. This can be problematic when your argument type isn't guaranteed e.g.
```
'[]' < 5 is false
[] < 5 is true
```

Qs4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```

This is an example of closures. Instantiating bar with a value of 1 creates a function with a local variable context that holds `a`. On the second call, the unnamed function is invoked with `b=2` and pulls in the variable context of it's parent `foo` including `a`. This returns the expected
 value of a (or 1) + b (or 2) = 3.

Qs5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

The above function can be used in cases where a blocking callback function (`done`) needs to be executed after 100ms.
For example, this could be used in cases where a doubling growth rate on a value of `a` needs to be computed and rendered concurrently.
