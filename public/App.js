const BACKEND_URL = 'http://localhost:4000';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleAPIErrors = this.handleAPIErrors.bind(this);
        // Debounce search API calls to minimize network requests and DB queryings
        this.getAndUpdateLocations = _.debounce(this.getAndUpdateLocations, 500);
        this.state = {
            search_query: '',
            search_results: [],
            user_errors: [],
        };
    }

    async handleAPIErrors(response) {
        const data = await response.json()
        if (!response.ok) {
            this.setState({
                user_errors: data.message
            })
            return [];
        }
        return data.locations;
    }

    getAndUpdateLocations() {
        // Call API only if 2 or more characters are in search query and check for invalid characters
        const regex = new RegExp("^[a-zA-Z]+$");

        if (this.state.search_query.length >= 2 && regex.test(this.state.search_query)) {
            fetch(`${BACKEND_URL}/locations?q=${this.state.search_query}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accepts': 'application/json'
                }
            })
                .then(this.handleAPIErrors)
                .then(locations => {
                
                    const ranked_locations = locations
                        // rank locations by similarity to search query
                        .map(loc => ({
                            name: loc.name,
                            latlong: `${loc.latitude}, ${loc.longitude}`,
                            rating: stringSimilarity.compareTwoStrings(this.state.search_query, loc.name)
                        }))
                        // sort using the rating key
                        .sort((a, b) => b.rating - a.rating)
                        // build an array of filtered location objects
                        .reduce((filtered, loc) => {
                            filtered.push({ name: loc.name, latlong: loc.latlong})
                            return filtered;
                        }, [])

                    this.setState({
                        search_results: ranked_locations,
                        user_errors: '',
                    })
                });
        } else {
            this.setState({
                user_errors: 'Search term must contain 2 or more alphabetic characters.',
                search_results: [],
            })
        }
    }

    handleInputChange(e) {
        this.setState({ search_query: e.target.value }, this.getAndUpdateLocations);
    }

    render() {
        return <div>
            <h1>GeoSearch App</h1>
            <form id="location-search-form">
                <label>
                    Enter Location:
                    <input type="text" name="locsearch" placeholder="Hastings" onChange={this.handleInputChange}></input>
                </label>
            </form>
            <h2>Results</h2>
            <ul id="search-results-list">
                {
                    this.state.search_results
                        .map((location, idx) => {
                        return <li key={idx}><b>{location.name}</b> - {location.latlong}</li>
                        })
                }
            </ul>
        </div>
    }
}

ReactDOM.render(<App />, document.querySelector('#App'));