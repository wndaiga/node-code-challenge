# GEOSEARCH APP

## SETUP
To start the project please run this `npm run start:fresh` command in your terminal:

This will setup and run an express app from port 4000 with the served react app accessible from
this url [http://localhost:4000].

## API
You can alternatively make API call requests to the server at the following endpoints:
- location data
  - url: /locations
  - example: /locations?q=hastings
  - data_format: json
  - notes: this api will only respond to valid queries that have 2 or more alphabet characters.

## TESTS
To execute the written tests, please run `npm test` in your terminal. Currently, only the locations
 API endpoint and utils tests have been written.