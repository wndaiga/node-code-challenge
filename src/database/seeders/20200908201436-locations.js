const csv = require('csv-parser');
const fs = require('fs');

const dataFilePath = './data/GB.tsv'

const fsStream = () => new Promise((resolve, reject) => {
  const results = [];

  fs.createReadStream(dataFilePath)
    .pipe(csv({
      separator: '\t',
      skipLines: 1,
      headers: [
        'geonameid',
        'name',
        'asciiname',
        'alternatenames',
        'latitude',
        'longitude',
        'feature_class',
        'feature_code',
        'country_code',
        'cc2',
        'admin1_code',
        'admin2_code',
        'admin3_code',
        'admin4_code',
        'population',
        'elevation',
        'dem',
        'timezone',
        'modification_date']
    }))
    .on('data', (data) => {
      const modData = Object.assign(data, { 'createdAt': Date.now(), 'updatedAt': Date.now() });
      return results.push(modData);
    })
    .on('end', () => {
      console.log('TSV file loading complete');
      resolve(results);
    })
    .on('error', (err) => {
      console.error('TSV file loading incomplete!', err);
      reject(err);
    });
});

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const locationData = await fsStream();
    const locQueryInterface = queryInterface.bulkInsert('locations', locationData, {});

    return locQueryInterface;
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('locations', null, {});
  }
};
