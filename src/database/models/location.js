'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class location extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  location.init({
    geonameid: DataTypes.INTEGER,
    name: DataTypes.STRING,
    asciiname: DataTypes.STRING,
    alternatenames: DataTypes.STRING,
    latitude: DataTypes.FLOAT,
    longitude: DataTypes.FLOAT,
    feature_class: DataTypes.STRING,
    feature_code: DataTypes.STRING,
    country_code: DataTypes.STRING,
    cc2: DataTypes.STRING,
    admin1_code: DataTypes.STRING,
    admin2_code: DataTypes.STRING,
    admin3_code: DataTypes.STRING,
    admin4_code: DataTypes.STRING,
    population: DataTypes.INTEGER,
    elevation: DataTypes.INTEGER,
    dem: DataTypes.INTEGER,
    modification_date: DataTypes.STRING,
    timezone: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'location',
  });
  return location;
};