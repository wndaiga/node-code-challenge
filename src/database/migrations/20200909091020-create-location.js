'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('locations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      geonameid: {
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      asciiname: {
        type: Sequelize.STRING
      },
      alternatenames: {
        type: Sequelize.STRING
      },
      latitude: {
        type: Sequelize.FLOAT
      },
      longitude: {
        type: Sequelize.FLOAT
      },
      feature_class: {
        type: Sequelize.STRING
      },
      feature_code: {
        type: Sequelize.STRING
      },
      country_code: {
        type: Sequelize.STRING
      },
      cc2: {
        type: Sequelize.STRING
      },
      admin1_code: {
        type: Sequelize.STRING
      },
      admin2_code: {
        type: Sequelize.STRING
      },
      admin3_code: {
        type: Sequelize.STRING
      },
      admin4_code: {
        type: Sequelize.STRING
      },
      population: {
        type: Sequelize.INTEGER
      },
      elevation: {
        type: Sequelize.INTEGER
      },
      dem: {
        type: Sequelize.INTEGER
      },
      modification_date: {
        type: Sequelize.STRING
      },
      timezone: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('locations');
  }
};