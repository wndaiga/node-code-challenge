const express = require('express');
const bodyParser = require('body-parser');
var locationsRouter = require('./routes/locations');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static("public"));

app.use('/locations', locationsRouter)

module.exports = app;