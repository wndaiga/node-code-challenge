const app = require('../app');
const assertDatabaseConnectionOk = require('../database/connection');
const { sequelize } = require('../database/models');
const { PORT } = require('../config');


async function server() {
	console.log(`Checking database connection...`);
	await assertDatabaseConnectionOk();

	console.log(`Syncing database...`);
	await sequelize.sync({})

	console.log(`Starting Node-Code-Challenge App on port ${PORT}...`);
	app.listen(PORT, () => {
		console.log(`Express server started on port ${PORT}.`);
	});
}

module.exports = server;