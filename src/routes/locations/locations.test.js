const request = require("supertest");
const app = require("../../app");

describe("Test locations endpoint", () => {
    test("It should return 400 status given no query in request", async () => {
        const response = await request(app).get("/locations");
        expect(response.statusCode).toBe(400);
    });
    test("It should not return location data with one character query requests", async () => {
        const response = await request(app).get("/locations?q=h");
        expect(response.body.locations).toStrictEqual([]);
    });
    test("It should not return location data with one non-alphabet character query requests", async () => {
        const response = await request(app).get("/locations?q=hast1ngs");
        expect(response.body.locations).toStrictEqual([]);
    });
    test("It should return location data with valid query requests", async () => {
        const response = await request(app).get("/locations?q=hastings");
        expect(response.statusCode).toBe(200);
    });
});
