const { searchLocationNameInDB } = require('../../utils/searchLocationNameInDB')
const { validateQuery } = require('../../utils/validateQuery')

// Search for locations
exports.location_search = function (req, res) {
    const { q } = req.query;

    if (!validateQuery(q)) {
        return res.status(400).send({
            locations: [],
            message: 
                `Search term must contain 2 or more alphabet characters. Read API documentation here - https://gitlab.com/squarenomad/node-code-challenge/-/blob/master/DOCUMENTATION.md`
        });
    }

    searchLocationNameInDB(q)
        .then(query_result => {
            if (query_result.ok) {
                return res.send({ locations: query_result.locations });
            }
        })
};
