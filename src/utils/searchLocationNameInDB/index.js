const { sequelize } = require('../../database/models');

exports.searchLocationNameInDB = async function (query) {
    const result = {
        locations: [],
        ok: true,
    };

    try {
        const sql = `SELECT name,latitude,longitude  FROM locations WHERE name LIKE '%${query}%'`;
        const query_result = await sequelize.query(sql);
        result.locations = query_result[0];
    } catch (err) {
        console.log(err.message);
        result.ok = false
    } finally {
        return result;
    }
};