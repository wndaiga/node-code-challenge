exports.validateQuery = function (query) {
    // Call API only if 2 or more characters are in search query and check for invalid characters

    // re-assign if query is falsy using short-circuit evaluation
    const queryString = query || '';

    if (queryString.length < 2) {
        return false;
    }

    const regexPattern = "^[a-zA-Z\s]+$";
    const regex = new RegExp(regexPattern);
    return regex.test(queryString);
};