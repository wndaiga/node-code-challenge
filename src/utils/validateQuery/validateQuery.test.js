const { validateQuery } = require('./');

describe("Test utility functions", () => {
    test("It should accept strings with more than 1 character", async () => {
        expect(validateQuery('ab')).toBe(true);
    });
    test("It should reject strings with less than 2 characters", async () => {
        expect(validateQuery('a')).toBe(false);
    });
    test("It should reject strings with numeric characters", async () => {
        expect(validateQuery('ab1')).toBe(false);
    });
    test("It should reject strings with special characters", async () => {
        expect(validateQuery('ab@')).toBe(false);
    });
    test("It should reject invalid types", async () => {
        expect(validateQuery(null)).toBe(false);
        expect(validateQuery([])).toBe(false);
    });
});
